# Commit message guidelines

## Setup

```
git clone git@gitlab.com:devtools/gittemplate.git ~/Git/gitTemplate
git config --global commit.template ~/Git/gitTemplate/template.txt
```

## Template

```
prefix: Short comment

Long comment with details
- item
- item
```

## Prefix convention

- **feat**: new feature
- **fix**: bug fix
- **dep**: dependency add or upgrade
- **docs**: documentation changes
- **style**: formatting, cosmetic changes
- **refactor**: code change that keeps functionality intact
- **perf**: performance improvement
- **test**: test update or addition
- **chore**: changes to build process or auxiliary tools
- **content**: changes on database/fixture/service contents
- **note**: add/update TODOs

## See also

https://github.com/sparkbox/how_to/tree/master/style/git

http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

http://karma-runner.github.io/0.8/dev/git-commit-msg.html
